﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SaisieValeur;

namespace SaisieValeurTests
{
    [TestClass]
    public class SaisieTest
    {
        [TestMethod]
        public void TestVerificationIfTrue()
        {
            int[] tab = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Assert.AreEqual(2, Program.Verification(tab, 2));
        }
        [TestMethod]
        public void TestVerificationIfalse()
        {
            int[] tab = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Assert.AreEqual(0, Program.Verification(tab, 11));
        }
    }
}