﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaisieValeur
{
    public class Program
    {
        public static int Verification(int[] valeurs, int valeur_user)
        {
            int present = 0, compteur1 = 0;
            while ((present == 0) && (compteur1 != 10))
            {
                if (valeur_user == valeurs[compteur1])
                {
                    present = compteur1 + 1;
                }
                compteur1++;
            }
            return present;
        }

        static void Main(string[] args)
        {
            int compteur, place, valeur_user;
            int[] valeurs = new int[10];
            Console.WriteLine("Saisissez 10 valeurs :");
            for (compteur = 0; compteur <= 9; compteur++)
            {
                Console.Write("{0} : ", compteur + 1);
                valeurs[compteur] = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("\nSaisissez une valeur :");
            valeur_user = int.Parse(Console.ReadLine());
            place = Verification(valeurs, valeur_user);

            if (place != 0)
            {
                Console.WriteLine("{0} est la {1}-eme valeur saisie", valeur_user, place);
            }
            else
            {
                Console.WriteLine("{0} n'apparait pas dans les valeurs saisies", valeur_user);
            }

            Console.ReadKey();
        }
    }
}