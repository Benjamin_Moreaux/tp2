﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CalculatriceConsole;

namespace CalculatriceConsoleTests
{
    [TestClass]
    public class OperationTest
    {
        [TestMethod]
        public void TestPonctuationIfTrue()
        {
            Assert.AreEqual(true, Program.Ponctuation("Je suis une phrase."));
        }

        [TestMethod]
        public void TestPonctuationIfFalse()
        {
            Assert.AreEqual(false, Program.Ponctuation("Je suis une phrase"));
        }

        [TestMethod]
        public void TestMajusculeIfTrue()
        {
            Assert.AreEqual(true, Program.Majuscule("Je suis une phrase."));
        }

        [TestMethod]
        public void TestMajusculeIfFalse1()
        {
            Assert.AreEqual(false, Program.Majuscule("je suis une phrase."));
        }

        [TestMethod]
        public void TestMajusculeIfFalse2()
        {
            Assert.AreEqual(false, Program.Majuscule("!Je suis une phrase."));
        }
    }
}