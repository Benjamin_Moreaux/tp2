﻿using System;

namespace CalculatriceConsole
{
    public class Program
    {
        public static bool Ponctuation(string phrase)
        {
            char lastlettre = phrase[phrase.Length - 1];
            char lettretest = '.';
            return lastlettre.Equals(lettretest);
        }

        public static bool Majuscule(string phrase)
        {
            char lettre1 = phrase[0];
            if ((lettre1 <= 'Z') && (lettre1 >= 'A'))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Saisissez une phrase :");
            string phrase = Console.ReadLine();
            Console.WriteLine("\n Vous avez saisi : \n{0}", phrase);
            bool Maj = Majuscule(phrase);
            bool Ponct = Ponctuation(phrase);
            if (Maj)
            {
                Console.WriteLine("\nCette phrase commence par une majscule.");
            }
            if (Ponct)
            {
                Console.WriteLine("\nCette phrase se termine par un point.");
            }

            Console.ReadKey();
        }
    }
}